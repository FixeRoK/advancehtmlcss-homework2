function toggleMenu() {
    let dropdown = document.getElementById("myDropdown");
    let toggleButton = document.getElementById("toggleButton");

    dropdown.classList.toggle("show");
    toggleButton.classList.toggle("active");

    if (dropdown.classList.contains("show")) {
        toggleButton.innerHTML = "&#10005;";
    } else {
        toggleButton.innerHTML = '<span class="toggle-button__bar cross"></span><span class="toggle-button__bar cross"></span><span class="toggle-button__bar cross"></span>';
    }
}

let menuItems = document.getElementsByClassName("header-mobile__link");
for (let i = 0; i < menuItems.length; i++) {
    menuItems[i].addEventListener("click", function () {
        let dropdown = document.getElementById("myDropdown");
        dropdown.classList.remove("show");

        let toggleButton = document.getElementById("toggleButton");
        toggleButton.classList.remove("active");
        toggleButton.innerHTML = '<span class="toggle-button__bar cross"></span><span class="toggle-button__bar cross"></span><span class="toggle-button__bar cross"></span>';
    });
}
